#ifndef EFF_H
#define EFF_H

struct eff *eff_open(const char *, const char *);
const char *eff_start(struct eff *);
void eff_fixedstart(struct eff *, const char *);
void eff_end(struct eff *);
uint32_t eff_getuint32(struct eff *);
uint16_t eff_getuint16(struct eff *);
uint8_t eff_getuint8(struct eff *);
char *eff_getstring(struct eff *);
void eff_openarray(struct eff *, size_t, size_t *);
void eff_getelement(struct eff *, size_t, void *);
void eff_closearray(struct eff *);
void eff_close(struct eff *);

struct effwriter *eff_create(const char *, const char *);
void eff_writestart(struct effwriter *, const char *);
void eff_writeend(struct effwriter *);
void eff_writeuint32(struct effwriter *, uint32_t);
void eff_writeuint16(struct effwriter *, uint16_t);
void eff_writeuint8(struct effwriter *, uint8_t);
void eff_writestring(struct effwriter *, const char *, size_t);
void eff_writearray(struct effwriter *, size_t, size_t, const void *);
void eff_done(struct effwriter *);

#if defined(EFF_INTERNAL)
#define block_type uint8_t
#define BT_END 0x0
#define BT_SHORTSTRING 0x1
#define BT_SHORTARRAY 0x2
#define BT_UINT8 0x3
#define BT_UINT16 0x4
#define BT_UINT32 0x5
#define BT_TYPE 0xF
#define BT_BLOCKSTART 0x10
/* 127 so larger type-ids can be encoded with high-bit = 1 */
#define BT_MAX 127

#if __BYTE_ORDER == __BIG_ENDIAN
#define u32trans(x)       (x)
#define u16trans(x)       (x)
#else
#if __BYTE_ORDER == __LITTLE_ENDIAN
#define u32trans(x)       __bswap_32(x)
#define u16trans(x)       __bswap_16(x)
#else
#error Unsupported endian or broken <endian.h>
#endif
#endif
#endif
#endif
