#ifndef TAGS_H
#define TAGS_H

extern int variable_count;
extern struct variable {
	struct variable *next;
	char *name;
	int id;
	bool usage[2];
} *switches;

struct tag {
	struct tag *next;
	char *name;
	struct pattern {
		struct pattern *next;
		char pattern[];
	} *patterns;
	int extractstart, extractend;
	unsigned char extractresetchar, extractstartchar, extractendchar;
	int id;
	int switch_set_action, switch_set_value;
	int switch_condition, switch_condition_min, switch_condition_max;
	enum mergemode {
		merge_none = 0,
		merge_same
	} mergemode;
	char *mergename1, *mergename2, *mergename3;
	int mergeid1, mergeid2, mergeid3;
};

extern struct tag *tags;

bool parse_tag_description(FILE *, const char *);
void tag_write(struct tag *, struct effwriter *);
#endif
