#ifndef GLOBALS_H
#define GLOBALS_H

void fail(const char *format, ...) __attribute__ ((noreturn, format (printf, 1, 2)));
void oom(void) __attribute__ ((noreturn));

#define zNEWvarstruct(num, type, field) ({type *__var = calloc(1, sizeof(type)+(num)*sizeof(__var->field[0])); if (__var == NULL) oom(); __var;})
#define NEW(num, type) ({type *__var = malloc((num)*sizeof(type)); if (__var == NULL) oom(); __var;})
#define zNEW(num, type) ({type *__var = calloc(num, sizeof(type)); if (__var == NULL) oom(); __var;})
#define RESIZEvarstruct(var_p, num, type, field) ({type *__var = realloc(*(var_p), sizeof(type)+(num)*sizeof(__var->field[0])); if (__var == NULL) oom(); *(var_p) = __var; __var ;})
#define RESIZE(var_p, num, type) ({type *__var = realloc(*(var_p), (num)*sizeof(type)); if (__var == NULL) oom(); *(var_p) = __var; __var; })

#endif
