REVOKE ALL ON DATABASE "qa-buildlogchecks" FROM public;
GRANT  ALL  ON  DATABASE  "qa-buildlogchecks"  TO  "qa-buildlogchecks";
GRANT  CONNECT ON  DATABASE  "qa-buildlogchecks"  TO  PUBLIC;

CREATE SCHEMA data;
REVOKE ALL ON SCHEMA data FROM PUBLIC;
GRANT  ALL ON SCHEMA data TO "qa-buildlogchecks";

-- CREATE TABLE available_sid ( package text, version text, architecture text, stamp text);
CREATE TABLE data.logs_sid (package text, version text, architecture text, stamp text, checkdone int);
CREATE TABLE data.tags_sid (package text, version text, architecture text, stamp text, tag text, data text, line int DEFAULT 0);
CREATE TABLE data.logs_experimental (package text, version text, architecture text, stamp text, checkdone int);
CREATE TABLE data.tags_experimental (package text, version text, architecture text, stamp text, tag text, data text, line int DEFAULT 0);

CREATE SCHEMA pub;
REVOKE ALL  ON SCHEMA pub FROM PUBLIC;
GRANT USAGE ON SCHEMA pub TO PUBLIC;

CREATE VIEW pub.tags_sid AS SELECT * FROM data.tags_sid;
GRANT SELECT on pub.tags_sid to PUBLIC;
CREATE VIEW pub.logs_sid AS SELECT * FROM data.logs_sid;
GRANT SELECT on pub.logs_sid to PUBLIC;
CREATE VIEW pub.stats_sid AS SELECT package, COUNT(CASE substr(tag,1,1) WHEN 'E' THEN tag ELSE NULL END) AS errors, COUNT(CASE substr(tag,1,1) WHEN 'W' THEN tag ELSE NULL END) AS warnings, COUNT(CASE substr(tag,1,1) WHEN 'I' THEN tag ELSE NULL END) AS infos, COUNT(CASE substr(tag,1,1) WHEN 'X' THEN tag ELSE NULL END) AS experimental FROM (SELECT package, tag, COUNT(1) AS count FROM data.tags_sid GROUP by package,tag) AS taglist WHERE tag != 'W-build-stamp-in-binary' OR count > 3 GROUP BY package;
GRANT SELECT on pub.stats_sid to PUBLIC;

CREATE VIEW pub.tags_experimental AS SELECT * FROM data.tags_experimental;
GRANT SELECT on pub.tags_experimental to PUBLIC;
CREATE VIEW pub.logs_experimental AS SELECT * FROM data.logs_experimental;
GRANT SELECT on pub.logs_experimental to PUBLIC;
CREATE VIEW pub.stats_experimental AS SELECT package, COUNT(CASE substr(tag,1,1) WHEN 'E' THEN tag ELSE NULL END) AS errors, COUNT(CASE substr(tag,1,1) WHEN 'W' THEN tag ELSE NULL END) AS warnings, COUNT(CASE substr(tag,1,1) WHEN 'I' THEN tag ELSE NULL END) AS infos, COUNT(CASE substr(tag,1,1) WHEN 'X' THEN tag ELSE NULL END) AS experimental FROM (SELECT package, tag, COUNT(1) AS count FROM data.tags_experimental GROUP by package,tag) as taglist WHERE tag != 'W-build-stamp-in-binary' OR count > 3 GROUP BY package;
GRANT SELECT on pub.stats_experimental to PUBLIC;
